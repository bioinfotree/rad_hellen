### phase_3.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# determine which shell the program must use
SHELL = /bin/bash

# use .flags to check completion of process_radtags
extern ../phase_3/SNP_FILT.vcf as SNP_FILT
extern ../phase_3/tags.bam as TAG_ALN
extern ../phase_3/tags.bai as TAG_ALN_IDX
extern ../phase_3/fake_genome.fna as REF


# alignments of paired reads on its paired contig consensus
# upload in IGV via File -> Load from File
# remember right click on sequences then:
#    Group alignments by -> sample
#    Sort alignments by -> sample
#    Color alignments by -> sample
TAGS_ALN.bam: $(TAG_ALN)
	ln -sf $< $@
# its index
TAGS_ALN.bai: $(TAG_ALN_IDX)
	ln -sf $< $@

# consensi of the paired contigs
# upload in IGV via File -> Load Genome from File
REFERENCE.fna: $(REF)
	ln -sf $< $@

# selected SNP
# upload in IGV via File -> Load from File
SNP_FILT.vcf: $(SNP_FILT)
	ln -sf $< $@



# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += SNP_FILT.vcf \
	 TAGS_ALN.bam \
	 TAGS_ALN.bai \
	 REFERENCE.fna

# The dependency of the targhet listened here
# are treate
# as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += SNP_FILT.vcf \
	 TAGS_ALN.bam \
	 TAGS_ALN.bai \
	 REFERENCE.fna

# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:

######################################################################
### phase_3.mk ends here
