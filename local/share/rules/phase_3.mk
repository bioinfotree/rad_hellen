### phase_3.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Aug 30 18:40:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# determine which shell the program must use
SHELL = /bin/bash

# use .flags to check completion of process_radtags
extern ../phase_1/sturgeons.flag as POOLS_DIR_FLAG
extern ../phase_1/gen_AN_fam.flag as AN_HAPLOTYPE_DIR_FLAG

# number of samples in the analysis
NUM_SAMPLES ?= 8
# number of reads required to output data for a particular stack
MOD_SORT_READ_PAIRS_MIN_READS ?= 15
# max kmer size for cortex binary
MAXK ?= 31
# max color size for cortex binary
NUM_COLS ?= 8
# name of cortex dir containing sources in $(PRJ_ROOT)/local/src
CORTEX_SRC ?= cortex_var_1.0.5.3
# cortex bin name
CORTEX_BIN ?= cortex_var_$(MAXK)_c$(NUM_COLS)
# kmer size actually used for build binaries
KMER_SIZE ?= 31
# fastq offset used for build binaries in cortex
FASTQ_OFFSET ?= 64
# max len of reads used for build binaries in cortex
MAX_READ_LEN ?= 120
# remove all supernodes where max coverage is <= the limit you set. Overrides --remove_seq_errors. Recommended method.
REMOVE_LOW_COVERAGE_SUPERNODES ?= 4
# specify the (estimated) genome size in bp. 
# Needed for estimate the genotype likelihood
GENOME_SIZE ?= 500
# name of fasta file containing all consents of contigs
FAKE_GENOME ?= fake_genome
# define mapping quality threshold to be used in process_calls.pl
MAPPING_QUAL_THRESH ?= 40






# link to demultiplexed samples dir from stacks
POOLS_DIR = $(basename $(notdir $(POOLS_DIR_FLAG)))
$(POOLS_DIR): $(POOLS_DIR_FLAG)
	ln -sf $(basename $<) ./$@

# link to genotype samples dir from stack
HAPLOTYPE_DIR = $(basename $(notdir $(AN_HAPLOTYPE_DIR_FLAG)))
$(HAPLOTYPE_DIR): $(AN_HAPLOTYPE_DIR_FLAG)
	ln -sf $(basename $<) ./$@


# log dir
log:
	mkdir -p $@;

# test
.PHONY: test
test:
	@echo $(CORTEX_SRC)/bin/$(notdir $(CORTEX_SRC)/bin/cortex_var_$(MAXK)_c$(NUM_COLS));


# aggregate into single dir per sample:
# -demultiplexed read 1 *.fq1
# -demultiplexed read 1 *.fq2
# -results from ustacks cstaks sstacks genotype
#  - *.alleles.tsv
#  - *.matches.tsv
#  - *.snps.tsv
#  - *.tags.tsv

# ../phase_1/sturgeons collect forward reads and paired reads divided by barcode and renamed
# ../phase_1/gen_AN_fam collect *.tags.tsv *.alleles.tsv *.snps.tsv file yelded by ustacks, and *.matches.tsv yelded by sstacks, and *.catalog.alleles.tsv, *.catalog.snps.tsv, *.catalog.tags.tsv yelded by cstacks, and *.haplotypes_1.tsv yelded by genotypes
classify_results.flag: $(POOLS_DIR) $(HAPLOTYPE_DIR)
	_comment () { echo -n ""; }; \
	_comment "take *.fq_1 file as reference for the name"; \
	for FULL_FILE in ./$</AN-*_*_*_*.fq_1; do \
	FILE=$$(basename "$$FULL_FILE"); \
	FILE_NAME=$${FILE%.*}; \
	DIR_NAME=$$FILE_NAME; \
	_comment "create directory"; \
	mkdir -p $$DIR_NAME && \
	cd $$DIR_NAME; \
	_comment "symlink to *.fq_1 from $(POOLS_DIR)"; \
	ln -sf ../$$FULL_FILE .; \
	_comment "symlink to *.fq_2 from $(POOLS_DIR)"; \
	ln -sf ../$</$$FILE_NAME.fq_2; \
	_comment "symlink to *.alleles.tsv from $(AN_HAPLOTYPE_DIR)"; \
	ln -sf ../$^2/$$FILE_NAME.alleles.tsv; \
	_comment "symlink to *.matches.tsv from $(AN_HAPLOTYPE_DIR)"; \
	ln -sf ../$^2/$$FILE_NAME.matches.tsv; \
	_comment "symlink to *.snps.tsv from $(AN_HAPLOTYPE_DIR)"; \
	ln -sf ../$^2/$$FILE_NAME.snps.tsv; \
	_comment "symlink to *.tags.tsv from $(AN_HAPLOTYPE_DIR)"; \
	ln -sf ../$^2/$$FILE_NAME.tags.tsv; \
	cd ..; \
	done; \
	touch $@



# only those loci that contain at least one SNP will be included in our genetic map, 
# create a whitelist of loci that we want to assemble: those that have SNPs. 
# This information is obtained from ../phase_1/gen_AN_fam contains *.haplotypes_1.tsv 
# yelded by genotypes from which I create a list of tags ID without header, 
# using first column
whitelist.tsv: $(HAPLOTYPE_DIR)
	cut -f 1 ./$</batch_1.haplotypes_1.tsv \
	| sed 1d > $@

# collate the paired-end sequences for each catalog locus. 
# use *.tags.tsv files (ustacks output) for each sample to know which reads were 
# assembled into which stacks, the *.matches.tsv files (sstacks output) 
# for each sample to know which stacks were matched to the same catalog locus, 
# for every stacks collect the paired-end reads, which will be grouped according to 
# which catalog locus they belong. Since stacks doesn't collect corresponding quality 
# for each paired reads, the script was slighty modified in order to bring also the 
# quality, returning paired sequenced in fastq format instead of fasta format

# The -r N parameter will only record loci that have at least N reads, 
# while the whitelist.tsv file will cause the program to only collate reads 
# for those loci listed in the file. 
collate_pairs.flag: classify_results.flag whitelist.tsv log
	for FULL_DIR in ./AN-*_*_*_*; do \
	DIR_NAME=$$(basename "$$FULL_DIR"); \
	mkdir -p pairs_$$DIR_NAME; \
	mod_sort_read_pairs -p $$FULL_DIR -s $$FULL_DIR -w $^2 -r $(MOD_SORT_READ_PAIRS_MIN_READS) -o ./pairs_$$DIR_NAME &> ./$^3/mod_sort_read_pairs.AN_fam.$$DIR_NAME.log; \
	done; \
	touch $@


# group together paired reads for tags from the different barcodes of parent_1
group_parent_1.flag: collate_pairs.flag
	mkdir -p pairs_AN-P1_LG402 && \
	for FULL_DIR in ./pairs_AN-P1_*_*_*; do \
	DIR_NAME=$$(basename "$$FULL_DIR"); \
	if [ -d "$$DIR_NAME" ]; then \
	if [ "$$(ls -A $$DIR_NAME)" ]; then \
	mv ./$$DIR_NAME/* pairs_AN-P1_LG402/; \
	fi; \
	rm -r $$DIR_NAME; \
	fi; \
	done; \
	touch $@

# group together paired reads for tags from the different barcodes of parent_2
group_parent_2.flag: collate_pairs.flag
	mkdir -p pairs_AN-P2_LG403 && \
	for FULL_DIR in ./pairs_AN-P2_*_*_*; do \
	DIR_NAME=$$(basename "$$FULL_DIR"); \
	if [ -d "$$DIR_NAME" ]; then \
	if [ "$$(ls -A $$DIR_NAME)" ]; then \
	mv ./$$DIR_NAME/* pairs_AN-P2_LG403/; \
	fi; \
	rm -r $$DIR_NAME; \
	fi; \
	done; \
	touch $@



# collate list of loci present in all samples
# put in a common list all tag IDs from all samples
# print unique tag IDs prefixed by number of occurrences
# shared loci will be those present a num of times equal to num samples
# filter for that
common_tags.lst: group_parent_1.flag group_parent_2.flag
	> all_tags.lst && \
	for FULL_DIR in ./pairs_AN-*_*; do \
	ls -1 $$FULL_DIR >> all_tags.lst; \
	done; \
	bsort --key=1,1 all_tags.lst \
	| uniq -c \
	| tr -s " " "\\t" \
	| bawk -v num_samples=$(NUM_SAMPLES) '!/^[$$,\#+]/ {  \
	if ($$2 >= num_samples) \
	{ print $$3;} \
	}' > $@; \
	rm -f all_tags.lst



# for each sample, collante in a new dir the paired files present in the common list.
# A symlink is created into the per sample new directory in order to track 
# the origin of the sample

# paired tags are renamed with a progressive number as prefix, that indicate the sample of origin
cortex_sources.lst: common_tags.lst
	_comment () { echo -n ""; }; \
	> $@; \
	i=1; \
	mkdir -p $(basename $@); \
	for FULL_DIR in ./pairs_AN-*_*; do \
	_comment "Increments the counter"; \
	ID="$$((i++))"; \
	DIR_NAME=$$(basename "$$FULL_DIR"); \
	cd $(basename $@); \
	echo -e "$$FULL_DIR\tST0$$ID" >> ../$@; \
	_comment "Iterate over line of file ../$< and use them name"; \
	while read line; do \
	if [ -s ../$$DIR_NAME/"$$line" ]; then \
	ln -sf  ../$$DIR_NAME/"$$line" st0$$ID\_$$line; \
	else \
	echo -e "File ../$$DIR_NAME/$$line not found\n"; \
	exit 1; \
	fi; \
	done < ../$<; \
	_comment "Loop over line of file finish here"; \
	cd ..; \
	_comment "test if more samples than expected"; \
	if (( "$$i"-1 > $(NUM_SAMPLES) )); then \
	echo -e "$$i samples found. $(NUM_SAMPLES) expected\n"; \
	exit 1; \
	fi; \
	done; \
	touch $@



# compile cortex binaries with the appropriate value of MAXK and numeber of colors NUM_COLS and link it into $(PRJ_ROOT)/local/bin/.

# Unfortunately version 1.0.5.3 doesn't compile on Ubuntu 12.04, also after having install libgsl0-dbg, gsl-bin, libgsl0-dev.
# What solve the problem is modify the makefile. Moved -lm -lgsl -lgslcblas  from the middle to the end of the recipe for the target cortex_var. Explicitly:

#  cortex_var : remove_objects $(CORTEX_VAR_OBJ) 
#         mkdir -p $(BIN); $(CC) $(CFLAGS_CORTEX_VAR) -lm -lgsl - 
# lgslcblas $(OPT) $(OPT_COLS) -o $(BIN)/cortex_var_$(join $ 
# (MAXK_AND_TEXT),$(NUMCOLS_AND_TEST)) $(CORTEX_VAR_OBJ)

#  to this 

#  cortex_var : remove_objects $(CORTEX_VAR_OBJ) 
#         mkdir -p $(BIN); $(CC) $(CFLAGS_CORTEX_VAR)  $(OPT) $ 
# (OPT_COLS) -o $(BIN)/cortex_var_$(join $(MAXK_AND_TEXT),$ 
# (NUMCOLS_AND_TEST)) $(CORTEX_VAR_OBJ) -lm -lgsl -lgslcblas 

CORTEX_BIN_PATH := $(PRJ_ROOT)/local/src/$(CORTEX_SRC)/bin/$(CORTEX_BIN)
$(CORTEX_BIN_PATH):
	_comment () { echo -n ""; }; \
	cd $(PRJ_ROOT)/local/src/$(CORTEX_SRC) && \
	_comment "compile cortex"; \
	$(MAKE) clean && \
	$(MAKE) -j $(CPUS) MAXK=$(MAXK) NUM_COLS=$(NUM_COLS) cortex_var && \
	cd $(PRJ_ROOT)/local/bin && \
	rm -f cortex_var_* && \
	_comment "link binary"; \
	ln -sf ../src/$(CORTEX_SRC)/bin/$(CORTEX_BIN) .


# builds single color binary for every paired tag. I.e.: fastq file
# for the flag --se_list cortex, require as argument a text file containing the path to the real fastq file.
# use process sobstitution in order to return such file
cortex_binaries.flag: $(CORTEX_BIN_PATH) cortex_sources.lst log
	if [[ -d $^3/$(basename $@) && ! -L $^3/$(basename $@) ]]; then \
	echo "Deleting existing directory $^3/$(basename $@)..."; \
	rm -r $^3/$(basename $@); \
	fi; \
	mkdir -p $^3/$(basename $@); \
	mkdir -p $(basename $@); \
	cd $(basename $^2); \
	for FILE in *.fq; do \
	$(notdir $<) \
	--se_list <(echo `readlink -sf $$FILE`) \
	--kmer_size $(KMER_SIZE) \
	--dump_binary ../$(basename $@)/"$${FILE%.fq}".ctx \
	--format FASTQ --fastq_offset $(FASTQ_OFFSET) \
	--max_read_len $(MAX_READ_LEN) \
	> ../$^3/$(basename $@)/"$${FILE%.fq}".log; \
	done; \
	cd ..; \
	touch $@



# print lists of binaries. Each list represents the path to a binary
cortex_binary_lsts.flag: cortex_binaries.flag
	mkdir -p $(basename $@); \
	cd $(basename $<); \
	for FILE in st*_*.ctx; do \
	echo `readlink -sf $$FILE` > ../$(basename $@)/"$${FILE%.*}".txt; \
	done; \
	cd ..; \
	touch $@


# create lists of colors that have to be compared to call bubbles within colors
# -every list correspond to a tag
# -every color in a list correspond to the current tag for a specific sample
# -every line of a list is a full path to a text file
# -every text file contains the full path to a binary prevuously created by cortex_var

# $${arrIN[i]}: print i element of array arrIN[i]
cortex_color_lsts.flag: cortex_binary_lsts.flag
	_comment () { echo -n ""; }; \
	_comment "test if dir exist and is not a symlink to dir"; \
	let THR=$(NUM_SAMPLES)+1; \
	if [[ -d $(basename $@) && ! -L $(basename $@) ]]; then \
	echo "Deleting existing directory $(basename $@)..."; \
	rm -r $(basename $@); \
	fi; \
	mkdir $(basename $@); \
	cd $(basename $<); \
	_comment "nice way to cicle using counter"; \
	COUNTER=1; \
	while [  $$COUNTER -lt $$THR ]; do \
	for FILE in st0$$COUNTER\_*.txt; do \
	_comment "split string using separator _. Result in array"; \
	arrIN=($${FILE//_/ }); \
	FILE_NAME=$${arrIN[1]%.*}; \
	echo `readlink -sf $$FILE` >> ../$(basename $@)/colz_$$FILE_NAME.txt; \
	done; \
	let COUNTER=COUNTER+1; \
	done; \
	cd ..; \
	touch $@


# call bubbles, from same tag, throught all samples
# for every bubble, one branch lie in a color, while the other lie in another color.
# use a statistical model for determining genotype likelihoods and for deciding
# if bubble is variants or repeats
# -1/-1 means no reference present
cortex_bubbles.flag: cortex_color_lsts.flag $(CORTEX_BIN_PATH) log
	mkdir -p $(basename $@); \
	if [[ -d $^3/$(basename $@) && ! -L $^3/$(basename $@) ]]; then \
	echo "Deleting existing directory $^3/$(basename $@)..."; \
	rm -r $^3/$(basename $@); \
	fi; \
	mkdir -p $^3/$(basename $@); \
	mkdir -p $(basename $@)_covg; \
	cd $(basename $<); \
	for FILE in colz_*.txt; do \
	$(notdir $^2) --colour_list $$FILE \
	--kmer_size $(KMER_SIZE) \
	--output_bubbles1 ../$(basename $@)/$(REMOVE_LOW_COVERAGE_SUPERNODES)_cleaned_bubbles\_"$${FILE%.txt}".txt \
	--detect_bubbles1 -1/-1 \
	--remove_low_coverage_supernodes $(REMOVE_LOW_COVERAGE_SUPERNODES) \
	--dump_covg_distribution ../$(basename $@)_covg/$(REMOVE_LOW_COVERAGE_SUPERNODES)_cleaned_bubbles_distribution\_"$${FILE%.txt}".txt \
	--experiment_type EachColourADiploidSample \
	--genome_size $(GENOME_SIZE) \
	--print_colour_coverages \
	> ../$^3/$(basename $@)/$(REMOVE_LOW_COVERAGE_SUPERNODES)_cleaned_bubbles_distribution\_"$${FILE%.txt}".log; \
	done; \
	cd ..; \
	touch $@


# records number of bubbles detected for every paired tag
count_bubbles.lst: cortex_bubbles.flag
	grep --directories=recurse --only-matching --initial-tab --with-filename --word-regexp --include=*_cleaned_bubbles_colz_*.txt --count 'Colour/sample' ./$(basename $<) \
	| tr ":" "\\t" > $@


# select files with one bubbles to reduce complexity (it's expected to have more bubbles than you need). Moreover having one and only one bubble in a contig leave more space to design primers nearby.

# concat_bubbles build a consensus read from a cortex bubble file containing one, and only one, bubble called. 

# the 5 prime, 1st bubble branch and 3 prime read of the bubble are concatenated. 

# it must be modified if you need to parse file with more than one bubble
$(FAKE_GENOME).fna: count_bubbles.lst cortex_bubbles.flag
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	_comment "filter for tags with only one bubble"; \
	LIST=`bawk '!/^[$$,\#+]/ {  \
	if ($$2 == 1) \
	{ print $$1;} \
	}' $< \
	| tr "\n" " "`; \
	cd $(basename $@); \
	for FILE in $$LIST; do \
	_comment "create link for every single bubble file"; \
	ln -sf .$$FILE . ; \
	FILE_NAME=$$(basename "$$FILE"); \
	_comment "recostruct consensus from bubble"; \
	concat_bubbles --input_file $$FILE_NAME --output_file "$${FILE_NAME%.txt}".fna; \
	done; \
	_comment "put together consents"; \
	cat *.fna > ../$@; \
	cd ..

# build a genome index file for stampy aligner
$(FAKE_GENOME).stidx: fake_genome.fna
	stampy -G $(basename $<) $<

# build a genome hash file for stampy aligner
$(FAKE_GENOME).sthash: $(FAKE_GENOME).stidx
	stampy -g $(basename $<) -H $(basename $<)


# put togethers all bubbles; needed by subsequent target
all_bubbles.txt: cortex_bubbles.flag
	cat ./$(basename $<)/*_colz_*.txt > $@


# transform variants from bubbles format to VCF format

# The script process_calls.pl is a modified version, probably by the cortex author.
# The script is very different from the original one from cortex 1.0.5.3 package
# It now has a menu using Getopt::Long moreover I changed the script that now can: 
# 1) find the stampy.py executable via $PATH
# 2) find the needleman_wunsch executable via $PATH 
#    (you must download and compile it from 
#    http://sourceforge.net/projects/needlemanwunsch/. 
#    You must make it accessible via $PATH)
# 3) the path to the folder of cortex is now provided as an input parameter
# 4) Statistics::Descriptive is now used instead of local modules

# 1) Take a file of Cortex bubbles as input. Requirement: must have used 
#    --print_colour_coverages
# 2) Map the 5prime flanks to a reference genome (use Stampy internally),
#    filter out calls where the mapping has quality <40.
# 3) For each call, align the two branches (alleles) against each other using  
#    full Needleman-Wunsch alignment algorithm. 
#    Parse the alignment to try to classify the call. 
#    This step has a tendency to falsely
#    classify a small number of indels as inversions (will be improved in future)
# 4) create 2 VCF files. 
#    -the "raw" VCF just gives the two alleles as called by Cortex in the VCF file 
#     (apart from trimming of the end of both alleles if they are identical). 
#    -the "decomposed" VCF attempts to split each call into it's constituent SNPs, 
#     indels etc.

# If you want to dig in and see which calls got filtered out, you can compare the VCF 
# with the original callfile and see which calls are missing
variants.flag: all_bubbles.txt cortex_sources.lst $(FAKE_GENOME).sthash $(FAKE_GENOME).stidx
	SAMPLES_LIST=`cut -f 2 $^2`; \
	mkdir -p $(basename $@); \
	cd $(basename $@); \
	process_calls --callfile ../$< \
	--outvcf $(basename $@) \
	--samplename_list <(echo -e $$SAMPLES_LIST | tr ' ' '\n') \
	--num_cols $(NUM_COLS) \
	--kmer $(KMER_SIZE) \
	--require_one_allele_is_ref no \
	--mapping_qual_thresh $(MAPPING_QUAL_THRESH) \
	--stampy_hash ../$(FAKE_GENOME) \
	--prefix HVS \
	--cortex_dir $(PRJ_ROOT)/local/src/$(CORTEX_SRC); \
	cd ..; \
	touch $@


# for every paired contig consensus in fasta format, here it's produced index and hash for subsequent realignment of its own reads, using stampy.
stampy_hashes.flag: $(FAKE_GENOME).fna log
	> log/stampy_hashes.log; \
	mkdir -p $(basename $@); \
	cd $(basename $@); \
	for FULL_PATH in ../$(FAKE_GENOME)/*_bubbles_colz_*.fna; do \
	FILE=$$(basename "$$FULL_PATH"); \
	FILE_NAME=$${FILE%.*}; \
	stampy -G $$FILE_NAME  $$FULL_PATH >> ../$^2/stampy_hashes.log 2>&1; \
	stampy -g $$FILE_NAME -H $$FILE_NAME >> ../$^2/stampy_hashes.log 2>&1; \
	done; \
	cd ..; \
	touch $@




# Now I realign reads related to paired contigs, from each sample, on them. 
# I need hashes for every selected paired contigs.
stampy_aligns.flag: $(FAKE_GENOME).fna cortex_sources.lst stampy_hashes.flag log
	_comment () { echo -n ""; }; \
	if [[ -d $^4/$(basename $@) && ! -L $^4/$(basename $@) ]]; then \
	echo "Deleting existing directory $^4/$(basename $@)..."; \
	rm -r $^4/$(basename $@); \
	fi; \
	mkdir -p $^4/$(basename $@); \
	mkdir -p $(basename $@); \
	cd $(basename $@); \
	_comment "cicle over consents of paired contigs"; \
	let THR=$(NUM_SAMPLES)+1; \
	for FULL_FILE in ../$(basename $<)/*_bubbles_colz_*.fna; do \
	_comment "extract tag ID from name"; \
	FILE=$$(basename "$$FULL_FILE"); \
	FILE_NAME=$${FILE%.*}; \
	arrIN=($${FILE_NAME//_/ }); \
	TAG_ID=$${arrIN[4]%.*}; \
	_comment "cicle on files into cortex_source dir by sample number. Those are fastq of paired reads named by sample name. Every cicle take all samples for same tag ID"; \
	COUNTER=1; \
	while [  $$COUNTER -lt $$THR ]; do \
	stampy --genome ../$(basename $^3)/$$FILE_NAME \
	--hash ../$(basename $^3)/$$FILE_NAME \
	--map ../$(basename $^2)/st0$$COUNTER\_$$TAG_ID.fq \
	--solexa \
	--sensitive \
	--baq \
	--outputformat sam \
	--output st0$$COUNTER\_$$TAG_ID.sam \
	--readgroup ID:st0$$COUNTER,LB:st0$$COUNTER,SM:st0$$COUNTER \
	--logfile ../$^4/$(basename $@)/st0$$COUNTER\_$$TAG_ID.log; \
	let COUNTER=COUNTER+1; \
	done; \
	done; \
	cd ..; \
	touch $@


# since all SAM alignments are put in the same dir, now categorize
# alignments by samples, in different directories with name from sample
.PHONY: classify_aligns
classify_aligns: stampy_aligns.flag
	let THR=$(NUM_SAMPLES)+1; \
	COUNTER=1; \
	while [ $$COUNTER -lt $$THR ]; do \
	mkdir -p st0$$COUNTER\_SAM; \
	cd st0$$COUNTER\_SAM; \
	ln -sf ../$(basename $<)/st0$$COUNTER\_*.sam .; \
	cd ..; \
	let COUNTER=COUNTER+1; \
	done; \


# join together alignments for the same tags across all samples
stampy_align4tags.flag: stampy_aligns.flag
	_comment () { echo -n ""; }; \
	mkdir -p $(basename $@); \
	cd $(basename $@); \
	_comment "cicle over tags of first sample. Other samples have same number of tags"; \
	let THR=$(NUM_SAMPLES)+1; \
	for FULL_FILE in ../$(basename $<)/st01_*.sam; do \
	_comment "extract tag ID from name"; \
	FILE=$$(basename "$$FULL_FILE"); \
	FILE_NAME=$${FILE%.*}; \
	arrIN=($${FILE_NAME//_/ }); \
	TAG_ID=$${arrIN[1]%.*}; \
	INPUT_LIST=""; \
	_comment "cicle on files into cortex_source dir by sample number. They are fastq of paired reads named by sample name. Every cicle take all samples for same tag ID"; \
	COUNTER=1; \
	while [ $$COUNTER -lt $$THR ]; do \
	_comment "generate list of input files for each tag"; \
	INPUT_LIST="$$INPUT_LIST INPUT=../$(basename $<)/st0$$COUNTER"_"$$TAG_ID.sam"; \
	let COUNTER=COUNTER+1; \
	done; \
	MergeSamFiles \
	VALIDATION_STRINGENCY=LENIENT \
	OUTPUT=merged\_$$TAG_ID.sam \
	SORT_ORDER=coordinate \
	VERBOSITY=ERROR \
	QUIET=true \
	$$INPUT_LIST; \
	done; \
	cd ..; \
	touch $@


# merge all tags in a unique sam file

# since all tags are more than 2400, this means more than 2400 SAM files.
# if try to merge all together in a single run, with picard tools, 
# you obtain following error:
# Exception in thread "main" net.sf.samtools.util.RuntimeIOException: java.io.FileNotFoundException: ./stampy_align4tags/merged_38472.sam (Too many open files)
# Therefore, it is necessary to merge the files progressively, in 2 steps. 
# In the first step join groups of 250 files. 
# In the second step, earlier groups are joined together in final SAM
tags.sam: stampy_align4tags.flag
	_comment () { echo -n ""; }; \
	TMP_DIR=`mktemp -d tmp_XXX`; \
	cd $$TMP_DIR; \
	_comment "split total list of files in pieces"; \
	ls -1 ../$(basename $<)/merged_*.sam \
	| split --lines 250 --numeric-suffixes - $@; \
	_comment "join SAMs within pieces"; \
	for FULL_PATH in $@*; do \
	INPUT_LIST=`bawk '!/^[$$,\#+]/ {  \
	printf "INPUT=%s ", $$0; \
	}' $$FULL_PATH`; \
	FILE=$$(basename "$$FULL_PATH"); \
	MergeSamFiles \
	VALIDATION_STRINGENCY=LENIENT \
	MERGE_SEQUENCE_DICTIONARIES=true \
	OUTPUT=$$FILE \
	SORT_ORDER=coordinate \
	VERBOSITY=ERROR \
	QUIET=true \
	CREATE_INDEX=false \
	$$INPUT_LIST; \
	done; \
	_comment "join earlier pieces"; \
	INPUT_LIST=""; \
	for FULL_PATH in $@*; do \
	INPUT_LIST="$$INPUT_LIST INPUT=$$FULL_PATH"; \
	done; \
	MergeSamFiles \
	VALIDATION_STRINGENCY=LENIENT \
	MERGE_SEQUENCE_DICTIONARIES=true \
	OUTPUT=../$@ \
	SORT_ORDER=coordinate \
	VERBOSITY=ERROR \
	QUIET=true \
	CREATE_INDEX=true \
	$$INPUT_LIST; \
	cd ..; \
	rm -r $$TMP_DIR;


# covert sam to bam and indicizze bam
# after conversion the tag "sample" into the alignments, becomes visible in IGV
tags.bam: tags.sam
	SamFormatConverter \
	VERBOSITY=ERROR \
	CREATE_INDEX=true \
	QUIET=true \
	INPUT=$< \
	OUTPUT=$@



# retain only SNPs: SVTYPE=SNP
SNP.vcf: variants.flag
	bawk '/^#/ {  \
	print $$0; \
	}' ./$(basename $<)/variants.decomp.vcf > $@; \
	grep --word-regexp 'SVTYPE=SNP' ./$(basename $<)/variants.decomp.vcf >> $@


# in some cases there are no reads supporting the reference allele nor the alternative
# allele, but the genotype is already called as homozygous with the reference allele 0/0:0,0:0.00. Instead, lookin at Helen results it should be: ./.:0,0:0.00 that means that no genotype are called (./.).
# to solve this problem, when the number of reads supporting each allele is 0, 
# "0,0", replace (0/0) with (./.)
genotypes.lst: SNP.vcf
	cut -f 1-3,10-17 $< \
	| awk '!/^[$$,\#+]/ { \
	printf "%s\t%i\t%s\t",$$1,$$2,$$3; \
	for (i=4; i <= NF; i++) \
	{ split($$i,a,":"); \
	# control if genotype not called \
	if (a[2] ~ "0,0") \
	{ a[1] = ".."; } \
	sub("/", "", a[1]); \
	printf "%s/",a[1]; \
	} \
	printf "\n"; \
	}' > $@


# take the genotypes list and filter for locus that are:
# -completely homozygous for alternative alleles in parents
# -olmost completely heterozygous in alla offsprings
# PARAMETERS:
# OPdw: minimum number of heterozygous offsprings
# OPup: maximum number of permitted homozygous offsprings
# P1s: start position of parent 1 alleles in genotype (0101010101011100)
# P1e: end position of parent 1 alleles
# P2s: start position of parent 2 alleles
# P2e: end position of parent 2 alleles
# Os: start position of offsprings alleles
# Oe: end position of offsprings alleles
# REMEMBER PARENTS ARE AT THE END: OF(01 01 01 01 01 01) P1(11) P2(00)
# in genotypes like: 00/01/../01/../../00/00/, ".." count as "00" but you can inspect
# genotypes.lst to understand if is true homozygous or not sequenced locus
FILT.lst: genotypes.lst
	bawk -v OPdw=5 -v OPup=3 -v P2s=15 -v P2e=16 -v P1s=13 -v P1e=14 -v Os=1 -v Oe=12 \
	'!/^[$$,\#+]/ { \
	gsub("/", "", $$4); \
	split($$4,a,""); \
	# test homozygosity in parents for alternative alleles \
	if ( ( a[P1e]+a[P1s] == 0 && a[P2e]+a[P2s] == 2 ) || ( a[P1e]+a[P1s] == 2 && a[P2e]+a[P2s] == 0 ) ) \
	{ \
	odd_sum=0; even_sum=0; \
	# sum first allele across every offspring \
	for (i=Os; i <= Oe; i=i+2) \
	{ \
	odd_sum=odd_sum+a[i]; \
	} \
	# sum second allele across every offspring \
	for (i=Os+1; i <= Oe; i=i+2) \
	{ \
	even_sum=even_sum+a[i]; \
	} \
	# test heterozygosity in affsprings \
	if ( ( odd_sum >= OPdw && even_sum <= OPup ) || ( odd_sum <= OPup && even_sum >= OPdw ) ) \
	{ print $$0,odd_sum,even_sum; } \
	} \
	}' $< > $@

# filter vcf for selected variants
# REMEMBER: for some SNPs the filter MAPQ (SNP filtered out because of low 
# mapping quality) was applied by process_calls (or stampy?)
# These SNP are still present in the list, but can be ignored by IGV
# when visualized on it
SNP_FILT.vcf: SNP.vcf FILT.lst
	bawk '/^#/ {  \
	print $$0; \
	}' $< > $@; \
	grep -f <(cut -f 1-3 $^2) --fixed-strings --word-regexp $< >> $@





# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += log \
	 classify_results.flag \
	 whitelist.tsv \
	 collate_pairs.flag \
	 group_parent_1.flag \
	 group_parent_2.flag \
	 common_tags.lst \
	 cortex_sources.lst \
	 $(CORTEX_BIN_PATH) \
	 cortex_binaries.flag \
	 cortex_binary_lsts.flag \
	 cortex_color_lsts.flag \
	 cortex_bubbles.flag \
	 count_bubbles.lst \
	 $(FAKE_GENOME).fna \
	 $(FAKE_GENOME).sthash \
	 $(FAKE_GENOME).stidx \
	 all_bubbles.txt \
	 variants.flag \
	 stampy_hashes.flag \
	 stampy_aligns.flag \
	 stampy_align4tags.flag \
	 tags.bam \
	 SNP.vcf \
	 genotypes.lst \
	 FILT.lst \
	 SNP_FILT.vcf




# The dependency of the targhet listened here
# are treate
# as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += tags.sam




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += $(POOLS_DIR) \
	 $(HAPLOTYPE_DIR) \
	 classify_results.flag \
	 whitelist.tsv \
	 collate_pairs.flag \
	 group_parent_1.flag \
	 group_parent_2.flag \
	 common_tags.lst \
	 cortex_sources.lst \
	 cortex_binaries.flag \
	 cortex_binary_lsts.flag \
	 cortex_color_lsts.flag \
	 cortex_bubbles.flag \
	 count_bubbles.lst \
	 $(FAKE_GENOME).fna \
	 $(FAKE_GENOME).sthash \
	 $(FAKE_GENOME).stidx \
	 all_bubbles.txt \
	 variants.flag \
	 stampy_hashes.flag \
	 stampy_aligns.flag \
	 stampy_align4tags.flag \
	 tags.bam \
	 tags.bai \
	 SNP.vcf \
	 genotypes.lst \
	 FILT.lst \
	 SNP_FILT.vcf




# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	$(RM) -r log
	$(RM) -r AN-*
	$(RM) -r pairs_*
	$(RM) -r cortex_*
	$(RM) -r fake_genome
	$(RM) -r variants
	$(RM) -r stampy_hashes
	$(RM) -r stampy_aligns
	$(RM) -r st0*_SAM
	$(RM) -r stampy_align4tags


######################################################################
### phase_3.mk ends here
