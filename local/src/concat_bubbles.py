#!/usr/bin/env python

from optparse import OptionParser
import sys,re


def get5prime(bubble_object):
     for line1 in bubble_object:
         if line1.startswith(">var_1_5"):
               line1 = bubble_object.next().rstrip()
               return line1


def getbranch1(bubble_object):
     for line2 in bubble_object:
         if line2.startswith(">branch_1_1"):
               line2 = bubble_object.next().rstrip()
               return line2 
 

def get3prime(bubble_object):
     for line3 in bubble_object:
         if line3.startswith(">var_1_3"):
               line3 = bubble_object.next().rstrip()
               return line3          



def main():
    #Setup options
    optparser=_prepare_optparser()
    (options, args) = optparser.parse_args()
    #verify options
    arg_pass=_verifyOption(options)
    if not arg_pass:
        print optparser.get_usage()
        print "Non valid arguments: exit"
        sys.exit(1)
    #if the output file name is not provided set it to input_file +suffix
    if not options.output_file:
        output_file=options.input_file+".out"
    else:
        output_file=options.output_file
    open_input_file = open(options.input_file)
    open_output_file = open(output_file, "w")
    
    contig = get5prime(open_input_file) + getbranch1(open_input_file) + get3prime(open_input_file)

    open_output_file.write(">tag_""%s\n"%options.input_file[0:-4])
    open_output_file.write("%s\n"%contig)
    
    open_input_file.close()
    open_output_file.close()



def _prepare_optparser():
    """Prepare optparser object. New options will be added in this function first.
    """
    usage = """usage: %prog this function will build a consensus read from a cortex bubble file containing one, and only one, bubble called """
    description = """The 5 prime, 1st bubble branch and 3 prime read of the bubble are concatenated."""
    
    prog_version="0.1"
    optparser = OptionParser(version="%prog v"+prog_version,description=description,usage=usage,add_help_option=False)
    optparser.add_option("-h","--help",action="help",help="show this help message and exit.")
    optparser.add_option("-i","--input_file",dest="input_file",type="string",
                         help="The input file to parse Default: %default")
    optparser.add_option("-o","--output_file",dest="output_file",type="string",
                         help="The output file with the contig. Default: %default")
    return optparser


def _verifyOption(options):
    """Check if the mandatory option are present in the options objects.
    @return False if any argument is wrong."""
    arg_pass=True
    
    if not options.input_file:
        print "You must specify an input file with -i"
        arg_pass=False
    return arg_pass


if __name__=="__main__":
    main()
