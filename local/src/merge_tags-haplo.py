#!/usr/bin/env python
from __future__ import with_statement

import sys
import argparse
from vfork.io.util import safe_rstrip
# provides regular expression matching operations
import re



def main(arguments):
	sys.argv = arguments
	args = arg_parse()

	stacks_tags = open(args.stacks_tags, "r")
		
	for line in open(args.stacks_hap, "r"):
		tokens = safe_rstrip(line).split("\t")
		tag_ID = int(tokens[0])

		found = False

		# more efficient while sorted bu tag ID
		while not found:
			line_2 = stacks_tags.readline()
			if not line_2:
				break
			tokens_2 = safe_rstrip(line_2).split("\t")
			tag_ID2 = int(tokens_2[2])
			if tag_ID == tag_ID2:
				unbarcoded_tag = tokens_2[9][6:]
				print "%i\t%s\t%s" %(tag_ID, unbarcoded_tag, "\t".join(
					tokens[3:]))
				found = True

		# for line_2 in open(args.stacks_tags, "r"):
		# 	tokens_2 = safe_rstrip(line_2).split("\t")
		# 	tag_ID2 = int(tokens_2[2])
		# 	if tag_ID == tag_ID2:
		# 		unbarcoded_tag = tokens_2[9][6:]
		# 		print "%i\t%s\t%s" %(tag_ID, unbarcoded_tag, "\t".join(
		#			tokens[3:]))
								
	return 0




def arg_parse():
    """
    Parses command line arguments
    """
    parser = argparse.ArgumentParser(description="Combines the lines of a stacks file *.catalog.tags.tsv with those of *.haplotypes.tsv, which have the same tag ID. Files must be first sorted by tag ID (using sort). Results to std-out.", prog=sys.argv[0], epilog="michele.vidotto@gmail.com")
    parser.add_argument("--haplotypes", "-a", dest="stacks_hap", required=True, help="stacks *.haplotypes.tsv file. Must be sorted by tag ID")
    parser.add_argument("--tags", "-t", dest="stacks_tags", required=True, help="stacks *.catalog.tags.tsv. Must be sorted by tag ID")

    args = parser.parse_args()
    
    return args


if __name__ == '__main__':
	main(sys.argv)

