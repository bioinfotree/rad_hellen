#!/usr/bin/env python
import re


def get5prime(bubble_object):
     for line1 in bubble_object:
         if line1.startswith(">var_1_5"):
               line1 = bubble_object.next().rstrip()
               return line1


def getbranch1(bubble_object):
     for line2 in bubble_object:
         if line2.startswith(">branch_1_1"):
               line2 = bubble_object.next().rstrip()
               return line2 
 

def get3prime(bubble_object):
     for line3 in bubble_object:
         if line3.startswith(">var_1_3"):
               line3 = bubble_object.next().rstrip()
               return line3          



def printconsensus(bubble_object):
    with open('bubble_object') as file1: 
        with open('out', 'w') as out1:
            print >> out1, '>concat' 
            print >> out1, get5prime(bubble_object) + getbranch1(bubble_object) + get3prime(bubble_object),






