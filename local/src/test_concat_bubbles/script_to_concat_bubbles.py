#!/usr/bin/env python
import re

f = open('example.txt')
concat = open("concat.fa", "w")

def get5prime(bubble_object):
     for line1 in bubble_object:
         if line1.startswith(">var_1_5"):
               line1 = bubble_object.next().rstrip()
               return line1


def getbranch1(bubble_object):
     for line2 in bubble_object:
         if line2.startswith(">branch_1_1"):
               line2 = bubble_object.next().rstrip()
               return line2 
 

def get3prime(bubble_object):
     for line3 in bubble_object:
         if line3.startswith(">var_1_3"):
               line3 = bubble_object.next().rstrip()
               return line3          


print '>concat\n get5prime(f) + getbranch1(f) + get3prime(f)',
 		

f.close()



